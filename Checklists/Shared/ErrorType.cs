﻿namespace Checklists.Shared;

public enum ErrorType
{
    BadRequest,
    NotFound,
    ServerError
}
