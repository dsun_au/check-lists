﻿using MongoDB.Bson.Serialization.Attributes;

namespace Checklists.Shared.Models;
public class CheckList
{
    [BsonElement("_id")]
    public Guid Id { get; init; } = Guid.NewGuid();
    public string Name { get; init; } = string.Empty;
    public CheckListItem[] CheckListItems { get; set; } = Array.Empty<CheckListItem>();
    public bool Completed { get; init; }
    public string[] SignedBy { get; init; } = Array.Empty<string>();
    public DateTime? Created { get; init; }
    public DateTime? Modified { get; init; }

}
