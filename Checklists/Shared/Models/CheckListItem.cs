﻿namespace Checklists.Shared.Models;
public class CheckListItem
{
    public string Name { get; set; } = "";

    public bool Checked { get; set; }

    public string Notes { get; set; } = "";

    public bool IsExpanded { get; set; } = true;

    public bool HasChild => ChildCheckListItems != null && ChildCheckListItems.Count > 0;

    public HashSet<CheckListItem> ChildCheckListItems { get; set; } = new HashSet<CheckListItem>();
}
