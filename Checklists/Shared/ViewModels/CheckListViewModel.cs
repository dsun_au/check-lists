﻿using Checklists.Shared.Models;

namespace Checklists.Shared.ViewModels;

public class CheckListViewModel
{
    public Guid Id { get; set; } = Guid.NewGuid();
    public string Name { get; set; } = string.Empty;
    public HashSet<CheckListItem> CheckListItems { get; set; } = new HashSet<CheckListItem>();
    public bool Completed { get; set; }
    public List<string> SignedBy { get; set; } = new List<string>();
    public DateTime? Created { get; set; }
    public DateTime? Modified { get; set; }
}
