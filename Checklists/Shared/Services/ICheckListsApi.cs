﻿using Checklists.Shared.ViewModels;
using Refit;

namespace Checklists.Shared.Services;
public interface ICheckListsApi
{
    [Get("/api/checkLists/{id}")]
    Task<IApiResponse<CheckListViewModel>> GetCheckList(Guid id);

    [Get("/api/checkLists")]
    Task<IApiResponse<CheckListViewModel[]>> GetAllCheckLists();

    [Put("/api/checkLists")]
    Task<IApiResponse<Guid>> SaveCheckList([Body]CheckListViewModel checkList);

    [Delete("/api/checkLists/{id}")]
    Task<IApiResponse<Guid>> DeleteCheckList(Guid id);

    [Get("/api/filledCheckLists/{id}")]
    Task<IApiResponse<CheckListViewModel>> GetFilledCheckList(Guid id);

    [Get("/api/filledCheckLists")]
    Task<IApiResponse<CheckListViewModel[]>> GetAllFilledCheckLists();

    [Put("/api/filledCheckLists")]
    Task<IApiResponse<Guid>> SaveFilledCheckList([Body] CheckListViewModel checkList);

    [Delete("/api/filledCheckLists/{id}")]
    Task<IApiResponse<Guid>> DeleteFilledCheckList(Guid id);

    [Post("/api/filledCheckLists/complete/{id}")]
    Task<IApiResponse<Guid>> CompleteFilledCheckList(Guid id);
}
