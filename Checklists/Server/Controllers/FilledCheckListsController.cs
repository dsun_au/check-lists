﻿using Checklists.Business.Interfaces;
using Checklists.Shared.Models;
using Checklists.Shared.ViewModels;
using Checklists.Utilities;
using Microsoft.AspNetCore.Mvc;

namespace Checklists.Server.Controllers;

[Route("api/[controller]")]
[ApiController]
public class FilledCheckListsController : ControllerBase
{
    private readonly IFilledCheckListService checkListService;

    public FilledCheckListsController(IFilledCheckListService checkListService)
    {
        this.checkListService = checkListService;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<CheckList>> GetCheckList([FromRoute] Guid id)
    {
        var getCheckListResult = await checkListService.GetCheckList(id);

        return getCheckListResult.ToActionResult();
    }

    [HttpGet]
    public async Task<ActionResult<CheckList[]>> GetAllCheckLists()
    {
        var getCheckListsResult = await checkListService.GetAllCheckLists();

        return getCheckListsResult.ToActionResult();
    }

    [HttpPut]
    public async Task<ActionResult<Guid>> PutCheckListTemplate([FromBody] CheckListViewModel checkList)
    {
        var result = await checkListService.SaveCheckList(checkList);
        return result.ToActionResult();
    }

    [HttpDelete("{id}")]
    public async Task<ActionResult<Guid>> DeleteCheckListTemplate([FromRoute] Guid id)
    {
        var result = await checkListService.DeleteCheckList(id);
        return result.ToActionResult();
    }

    [HttpPost("complete/{id}")]
    public async Task<ActionResult<Guid>> CompleteCheckList(Guid id)
    {
        var result = await checkListService.CompleteCheckList(id);
        return result.ToActionResult();
    }
}
