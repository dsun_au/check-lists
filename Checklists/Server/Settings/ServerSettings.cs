﻿namespace Checklists.Server.Settings;

public class ServerSettings
{
    public ServerSettings()
    {

    }
    public string MongoConnectionString { get; init; } = "";

    public string MongoDatabase { get; init; } = "";
};
