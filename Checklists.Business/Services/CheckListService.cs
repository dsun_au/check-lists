﻿using Checklists.Business.Interfaces;
using Checklists.Business.Settings;
using Checklists.Shared;
using Checklists.Shared.Models;
using Checklists.Shared.ViewModels;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace Checklists.Business.Services;

public abstract class CheckListService : ICheckListService, ITemplateCheckListService
{
    private readonly IMongoDatabase mongoDatabase;
    protected readonly CheckListServiceSettings serviceSettings;
    private readonly FilterDefinitionBuilder<CheckList> filterBuilder = new();
    private readonly UpdateDefinitionBuilder<CheckList> updateBuilder = new();

    public CheckListService(IMongoDatabase mongoDatabase, IOptions<CheckListServiceSettings> serviceSettings)
    {
        this.mongoDatabase = mongoDatabase;
        this.serviceSettings = serviceSettings.Value;
    }

    protected abstract string CollectionName { get; }
    protected IMongoCollection<CheckList> GetCollection() => mongoDatabase.GetCollection<CheckList>(CollectionName);

    public async Task<IResult<CheckList>> GetCheckList(Guid id)
    {
        var collection = GetCollection();

        var filter = filterBuilder.Eq(checkList => checkList.Id, id);

        var cursor = await collection.FindAsync(filter);
        var checkLists = cursor.ToList();

        return checkLists.Count == 0 || checkLists.Single() == null
            ? new Result<CheckList>(new[] {"Couldn't find checklist."}, ErrorType.NotFound)
            : new Result<CheckList>(checkLists.Single());
    }

    public async Task<IResult<CheckList[]>> GetAllCheckLists()
    {
        var collection = GetCollection();
        var cursor = await collection.FindAsync(filterBuilder.Empty);
        var list = await cursor.ToListAsync();

        return new Result<CheckList[]>(list.ToArray());
    }

    public async Task<IResult<Guid>> SaveCheckList(CheckListViewModel checkListViewModel)
    {
        var checkList = MapViewModelToModel(checkListViewModel);

        var collection = GetCollection();

        var result = await collection.ReplaceOneAsync(doc => doc.Id == checkList.Id, checkList, new ReplaceOptions { IsUpsert = true });
        
        return result.UpsertedId is not null || result.ModifiedCount > 0
            ? new Result<Guid>(checkList.Id)
            : new Result<Guid>(new[] { "Save failed." }, ErrorType.ServerError);
    }

    public async Task<IResult<Guid>> DeleteCheckList(Guid id)
    {
        var collection = GetCollection();

        var filter = filterBuilder.Eq(checkList => checkList.Id, id);

        var result = await collection.DeleteOneAsync(filter);

        return result.DeletedCount == 0
            ? new Result<Guid>(new[] { "Couldn't delete checklist." }, ErrorType.NotFound)
            : new Result<Guid>(id);
    }

    protected static CheckList MapViewModelToModel(CheckListViewModel viewModel) =>
        new()
        {
            Id = viewModel.Id == default ? Guid.NewGuid() : viewModel.Id,
            Name = viewModel.Name,
            CheckListItems = viewModel.CheckListItems.ToArray(),
            Completed = viewModel.Completed,
            Created = viewModel.Created ?? DateTime.UtcNow,
            Modified = viewModel.Created is not null ? DateTime.UtcNow : null,
            SignedBy = viewModel.SignedBy.ToArray(),
        };
    


}
