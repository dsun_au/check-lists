﻿using Checklists.Business.Interfaces;
using Checklists.Business.Settings;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.Core.Configuration;

namespace Checklists.Business.Services;
public class TemplateCheckListService : CheckListService, ITemplateCheckListService
{
    public TemplateCheckListService(IMongoDatabase mongoDatabase, IOptions<CheckListServiceSettings> serviceSettings) : base(mongoDatabase, serviceSettings)
    {
    }

    protected override string CollectionName => serviceSettings.TemplateCollectionName;

}
