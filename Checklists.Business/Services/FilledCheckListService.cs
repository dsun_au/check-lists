﻿using Checklists.Business.Interfaces;
using Checklists.Business.Settings;
using Checklists.Shared;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Driver.Core.Configuration;

namespace Checklists.Business.Services;
public class FilledCheckListService : CheckListService, IFilledCheckListService
{
    public FilledCheckListService(IMongoDatabase mongoDatabase, IOptions<CheckListServiceSettings> serviceSettings) : base(mongoDatabase, serviceSettings)
    {
    }

    protected override string CollectionName => serviceSettings.FilledCollectionName;

    public Task<IResult<Guid>> CompleteCheckList(Guid id)
    {
        // Send email, mark as completed?

        return Task.FromResult((IResult<Guid>)(new Result<Guid>(id)));
    }
}
