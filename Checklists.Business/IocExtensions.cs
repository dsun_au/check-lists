﻿using Checklists.Business.Interfaces;
using Checklists.Business.Services;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Driver.Core.Configuration;

namespace Checklists.Business;
public static class IocExtensions
{
    public static void AddBusinessServices(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddSingleton<ITemplateCheckListService, TemplateCheckListService>();
        serviceCollection.AddSingleton<IFilledCheckListService, FilledCheckListService>();
    }
}
