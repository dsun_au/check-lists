﻿using Checklists.Shared.Models;
using Checklists.Shared;
using Checklists.Shared.ViewModels;

namespace Checklists.Business.Interfaces;
public interface ICheckListService
{
    Task<IResult<CheckList[]>> GetAllCheckLists();
    Task<IResult<CheckList>> GetCheckList(Guid id);
    Task<IResult<Guid>> SaveCheckList(CheckListViewModel checkList);
    Task<IResult<Guid>> DeleteCheckList(Guid id);
}
