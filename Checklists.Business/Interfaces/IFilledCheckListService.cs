﻿using Checklists.Shared.Models;
using Checklists.Shared;

namespace Checklists.Business.Interfaces;
public interface IFilledCheckListService : ICheckListService
{
    Task<IResult<Guid>> CompleteCheckList(Guid id);
}
