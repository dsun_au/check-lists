﻿namespace Checklists.Business.Settings;
public class CheckListServiceSettings
{
    public string TemplateCollectionName { get; init; } = "";
    public string FilledCollectionName { get; init; } = "";

}
